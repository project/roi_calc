INTRODUCTION
------------

The ROI Calculator module provide the Asset Tracking Calculator displays the form including many fields. 
After filling out the form fields the user will get some initial feedback about their ROI.
They will ultimately be prompted to enter their email and first and last name where they will get a dynamically generated pdf report based off their answers.
THis report showing numbers and graphs of how the investment of an asset tracking program will help their business.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/roi_calc

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/roi_calc


REQUIREMENTS
------------

This module requires the following modules:

 * ctools (https://drupal.org/project/ctools)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, ROI calculator form will be display on roi-calcualtor link.
