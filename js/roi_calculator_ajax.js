(function ($) {
  /**
   * Special Effects AJAX framework command.
   */
  Drupal.ajax.prototype.commands.roiCalculator =  function(ajax, response, status) {
    // focus the element after ajax callback
    var element_name = $(ajax.element).attr('name');
    var form_wrapper = $('[name="'+element_name+'"]').parents('#roi-form-wrapper');
    textboxes = $(form_wrapper).find('input:text');
    if(element_name == 'main_container[cost_asset_searches][person_days_per_year]') {
      $('html, body').animate({
        scrollTop: $('[name="main_container[cost_asset_searches][person_days_per_year]"]').offset().top + 80
      }, 500);
    }
    else {
      $(textboxes).each(function(index) {
        currentBoxNumber = index;
        if($(this).attr('name') == element_name) {
          nextBox = textboxes[index + 1];
          if($(nextBox).is(":disabled")) {
            nextBox = textboxes[currentBoxNumber + 2];
            if($(nextBox).is(":disabled")) {
              nextBox = textboxes[currentBoxNumber + 3];
            }
          }
          if (nextBox != null) {
            nextBox.focus();
          }
        }
      })
    }
    
    // var next_element = $('[name="'+element_name+'"]').closest('div').next('div').find('input:text');
    // if($(next_element).is(":disabled")) {
    //   console.log('yes');
    //   // $('[name="'+element_name+'"]').closest('div').next('div').find('input:text')
    // }
    // $('[name="'+element_name+'"]').closest('div').next('div').find('input:text').focus();
    // $('html, body').animate({
    //   scrollTop: $('[name="'+element_name+'"]').offset().top - 150
    // }, 500);
  }
})(jQuery);