(function ($) {
	Drupal.theme.prototype.roi_report_modal = function () {
	  var html = ''
	  html += '<div id="ctools-modal">'
	  html += '  <div class="ctools-modal-content ctools-modal-save-label-modal-content">' // panels-modal-content
	  html += '    <div class="modal-header">';
	  html += '      <a class="close" href="#">';
	  html +=          Drupal.CTools.Modal.currentSettings.closeImage;
	  html += '      </a>';
	  html += '      <span id="modal-title" class="modal-title">&nbsp;</span>';
	  html += '    </div>';
	  html += '    <div id="modal-content" class="modal-content">';
	  html += '    </div>';
	  html += '  </div>';
	  html += '</div>';

      return html;
	}
})(jQuery);