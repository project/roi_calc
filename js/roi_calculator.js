(function ($) {
    Drupal.behaviors.roiCalculator = {
      attach: function (context, settings) {
      	if($('.page-roi-calculator').length > 0) {
          // Overlay global throbber HTML,
          // this is what we actually show for the end user
          var div = '<div id="global-throbber"><div class="ajax-progress-throbber"><div class="throbber"></div></div></div>';

          // jQuery Ajax Events need to be triggered on the document element
          // This is triggered on the beginning of an jQuery AJAX request
          $(document).ajaxSend(function( event, request, settings ) {
            // Add throbber on system or views ajax calls
            // Most core and contrib module callbacks will use these urls.
              if (settings.url == "/system/ajax" || settings.url == "/label_outlet_new/label-outlet-metalcraft/system/ajax") {
                // If not yet added, if multiple AJAX requests are fired
                if ($('#global-throbber').length == 0) {
                  // Add the overlay div to the beginning of the page,
                  // and fade it in halfway
                  $('body').append(div);
                  $('#global-throbber').fadeTo("fast" , 0.5);
                }
              }
              $("input[disabled!='disabled']").each(function() {
                if ($(this).css("display") !== "none" && $(this).attr("type") == 'submit') {
                  // Disable the elements, and add a class (global-throbber-disabled)
                  // The class is for enabling the elements on completion.
                  // Also add a disabled class for theming
                  $(this).attr("disabled", true).addClass("global-throbber-disabled").addClass('disabled');
                }
              });
          });

          // Again, needs to be triggered on the document element
          // This is triggered on the completion of an jQuery AJAX request
          $(document).ajaxComplete(function( event, request, settings ) {
            // Enable all disabled elements, by using the class
            // that we added on the ajaxSend Event.
            // Then we can remove the class that we added for this procedure
            // We added a disabled class for theming, also remove that.
              $("input.global-throbber-disabled").removeAttr("disabled","disabled").removeClass("global-throbber-disabled").removeClass('disabled');
              // Remove throbber container added to the beginning of the body
              $('#global-throbber').remove();
          });
        }

      	//logic to shift on next element of form after Enter press
   		var currentBoxNumber = 0;
		  $("#roi-form-wrapper .form-type-textfield input").keyup(function (event) {
		    if (event.keyCode == 13) {
            textboxes = $("#roi-form-wrapper .form-type-textfield input");
            if($(this).attr('name') == 'main_container[cost_asset_searches][person_days_per_year]') {
              $('#submit-btn').focus();
            }
            else { 
              currentBoxNumber = textboxes.index(this);
              nextBox = textboxes[currentBoxNumber + 1];
              if($(nextBox).is(":disabled")) {
                  nextBox = textboxes[currentBoxNumber + 2];
                  if($(nextBox).is(":disabled")) {
                    nextBox = textboxes[currentBoxNumber + 3];
                  }
              }
              if (nextBox != null) {
                  nextBox.focus();
                  event.preventDefault();
                  return false;
              }
            }
		        
		    }
		  });
    }
  }
})(jQuery);