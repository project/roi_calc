<?php

/**
 * @file
 * Template for ROI Report.
 */
$roivalues = !empty($roivalues) ? $roivalues : NULL;

?>

<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            <?php print $inline_css; ?>
        </style>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
    <body>
        <?php print roi_calculator_html($roivalues); ?>
    </body>
</html>
